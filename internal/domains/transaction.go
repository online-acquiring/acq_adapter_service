package domains

type Transact struct {
	ID       string
	AgentID  string
	LeadID   string
	SUM      string
	State    int
	OperDate string
	Purpose  string
}

type Agent struct {
	ID   string
	Tax  string
	Name string
}
