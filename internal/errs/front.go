package errs

import "errors"

const (
	ErrorCodeInternalServer = iota + 1000
	ErrorCodeTaxNotFound
	ErrorCodeTransactNotFound
)

var (
	InternalError    = NewError(ErrorCodeInternalServer, "Internal server error")
	TaxNotFound      = NewError(ErrorCodeTaxNotFound, "There is not a tax")
	TransactNotFound = NewError(ErrorCodeTransactNotFound, "There is not transact")
)

var (
	ErrNoRows = errors.New("no rows in result set")
)
