package errs

import (
	"fmt"
	// errs "github.com/go-errs/errs"
)

type Error struct {
	Err     error
	Code    int
	Message string
}

func (e *Error) Error() string {
	return fmt.Sprintf("%v %v", e.Code, e.Message)
}

func (e *Error) Msg() string {
	return e.Message
}

func (e *Error) ErrCode() int {
	return e.Code
}

func NewError(code int, msg string) *Error {
	return &Error{
		Code:    code,
		Message: msg,
	}
}

func Errf(err error, tmpl string, args ...interface{}) error {
	return &Error{Err: err, Message: fmt.Sprintf(tmpl, args...)}
}
