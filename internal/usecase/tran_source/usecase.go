package transource

import (
	"context"
)

type tranSource interface {
	Create(ctx context.Context, tr domains.Transact) (domains.Transact, error)
	Get(ctx context.Context, tr domains.Transact) (domains.Transact, error)
	Update(ctx context.Context, tr domains.Transact) error
	CheckAgent(ctx context.Context, ag domains.Agent) (domains.Agent, error)
	CreateAgent(ctx context.Context, ag domains.Agent) (domains.Agent, error)
}

type UseCase struct {
	tranSource tranSource
}

func New(tr tranSource) *UseCase {
	return &UseCase{
		tranSource: tr,
	}
}

func (uc *UseCase) CreateTransact(ctx context.Context, tr domains.Transact) (domains.Transact, error) {
	return uc.tranSource.Create(ctx, tr)
}

func (uc *UseCase) Get(ctx context.Context, tr domains.Transact) (domains.Transact, error) {
	return uc.tranSource.Get(ctx, tr)
}

func (uc *UseCase) UpdateState(ctx context.Context, tr domains.Transact) error {
	return uc.tranSource.Update(ctx, tr)
}

func (uc *UseCase) CheckAgent(ctx context.Context, ag domains.Agent) (domains.Agent, error) {
	agent, err := uc.tranSource.CheckAgent(ctx, ag)
	if err != nil {
		return domains.Agent{}, err
	}

	if agent.ID == "" {
		return domains.Agent{}, errs.TaxNotFound
	}

	return agent, nil
}

func (uc *UseCase) CreateAgent(ctx context.Context, ag domains.Agent) (domains.Agent, error) {
	return uc.tranSource.CreateAgent(ctx, ag)
}

func (uc *UseCase) CheckOrCreateAgent(ctx context.Context, ag domains.Agent) (domains.Agent, error) {
	agent, err := uc.tranSource.CheckAgent(ctx, ag)
	if err != nil {
		return domains.Agent{}, err
	}

	if agent.ID == "" {
		agent, err = uc.tranSource.CreateAgent(ctx, ag)
		if err != nil {
			return domains.Agent{}, err
		}
	}

	return agent, nil
}
