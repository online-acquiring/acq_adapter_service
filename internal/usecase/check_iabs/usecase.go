package iabs

import (
	"context"
)

type tranSource interface {
	CreateTransact(ctx context.Context, tr domains.Transact) (domains.Transact, error)
	CheckOrCreateAgent(ctx context.Context, ag domains.Agent) (domains.Agent, error)
}

type railSource interface {
	SendTransactID(ctx context.Context, tr domains.Transact, ag domains.Agent) error
}

type UseCase struct {
	tranSource tranSource
	railSource railSource
}

func New(tr tranSource, rl railSource) *UseCase {
	return &UseCase{
		tranSource: tr,
		railSource: rl,
	}
}

func (uc *UseCase) CheckFromIabs(ctx context.Context, tran domains.Transact) error {
	var l = logger.FromCtx(ctx, "uc.check_iabs.CheckFromIabs")
	agent := domains.Agent{
		Tax: tran.AgentID,
	}

	ag, err := uc.tranSource.CheckOrCreateAgent(ctx, agent)
	if err != nil {
		l.Error(err.Error())

		return err
	}

	tran.AgentID = ag.ID
	tr, err := uc.tranSource.CreateTransact(ctx, tran)
	if err != nil {
		l.Error(err.Error())

		return err
	}

	err = uc.railSource.SendTransactID(ctx, tr, ag)
	if err != nil {
		l.Error(err.Error())

		return err
	}

	return nil
}
