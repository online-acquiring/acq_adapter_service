package adapter

import (
	"context"
)

type tranSource interface {
	Get(ctx context.Context, tr domains.Transact) (domains.Transact, error)
	UpdateState(ctx context.Context, tr domains.Transact) error
	CheckAgent(ctx context.Context, ag domains.Agent) (domains.Agent, error)
}

type UseCase struct {
	tranSource tranSource
}

func New(tr tranSource) *UseCase {
	return &UseCase{
		tranSource: tr,
	}
}

func (uc *UseCase) CheckFromAdapter(ctx context.Context, tran domains.Transact, ag domains.Agent) (domains.Transact, *errs.Error) {
	var l = logger.FromCtx(ctx, "uc.check_adapter.CheckFromAdapter")
	agent, err := uc.tranSource.CheckAgent(ctx, ag)
	if err != nil {
		return domains.Transact{}, errs.TaxNotFound
	}

	tr, err := uc.tranSource.Get(ctx, domains.Transact{ID: tran.ID})
	if err != nil {
		l.Error(err.Error())

		return domains.Transact{}, errs.TransactNotFound
	}

	err = uc.tranSource.UpdateState(context.Background(), domains.Transact{ID: tr.ID})
	if err != nil {
		l.Error(err.Error())

		return domains.Transact{}, errs.NewError(errs.ErrorCodeInternalServer, err.Error())
	}

	tr.AgentID = agent.Tax

	return tr, nil
}
