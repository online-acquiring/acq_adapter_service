package services

import (
	"crypto/tls"
	"net/http"
)

func GetOpenAPIMiddleware(cfg config.Config) remote.Middleware {
	cred := remote.WSOAMCredentials{
		Key:    cfg.OpenAPI.CustomerKey,
		Secret: cfg.OpenAPI.CustomerSecret,
		URL:    cfg.OpenAPI.AuthURL,
	}
	var cf = tls.Config{
		InsecureSkipVerify: true, //nolint
	}
	m := remote.NewWSO2AMMiddleware(cred, &http.Transport{TLSClientConfig: &cf})

	return m
}
