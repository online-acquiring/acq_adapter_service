package railway

type Request struct {
	ID     string `json:"id"`
	Method string `json:"method"`
	Params Param  `json:"params"`
}

type Param struct {
	TransactionID string `json:"transactionId"`
	Inn           string `json:"inn"`
	Amount        string `json:"amount"`
	Purpose       string `json:"purpose"`
}

type Response struct {
	ID     string `json:"id"`
	Error  *Error `json:"error"`
	Result Result `json:"result"`
}

func (r Response) ToError() error {
	return nil
}

func (r Response) Scan(i interface{}) error {
	return nil
}

type Result struct {
	TransactionID string `json:"transactionId"`
	State         int    `json:"state"`
}

type Error struct {
	State   int    `json:"state"`
	Message string `json:"message"`
}
