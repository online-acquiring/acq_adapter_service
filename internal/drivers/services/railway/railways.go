package railway

import (
	"context"
	"crypto/sha512"
	"crypto/tls"
	"encoding/base64"
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/google/uuid"
	"go.uber.org/zap"
)

type Client struct {
	httpClient remote.HTTPClientI
	cfg        config.Config
}

func New(cfg config.Config) *Client {
	var cf = tls.Config{
		InsecureSkipVerify: true, //nolint
	}
	client := remote.New(cfg.OpenAPI.Host, &http.Transport{
		TLSClientConfig: &cf,
	})

	client.SetMiddleware(services.GetOpenAPIMiddleware(cfg))

	return &Client{
		httpClient: client,
		cfg:        cfg,
	}
}

func (c *Client) SendTransactID(ctx context.Context, tr domains.Transact, ag domains.Agent) error {
	var (
		l    = logger.FromCtx(ctx, "railways.SendTransactID")
		resp Response
	)

	req := Request{
		ID:     uuid.New().String(),
		Method: method,
		Params: Param{
			TransactionID: tr.ID,
			Inn:           ag.Tax,
			Amount:        tr.SUM,
			Purpose:       tr.Purpose,
		},
	}

	timeN := strconv.FormatInt(time.Now().Unix(), 10)
	mH := make(map[string]string)
	mH["userId"] = c.cfg.RailWayUserID
	mH["timestamp"] = timeN
	input := c.cfg.RailWaySecret + timeN + c.cfg.RailWayUserID
	sha := sha512.New()
	sha.Write([]byte(input))
	mH["OTP"] = base64.StdEncoding.EncodeToString(sha.Sum(nil))

	// add 15 sec timeout on request
	ctxT, cancel := context.WithTimeout(ctx, time.Second*defaultTimeOut)
	defer cancel()

	err := c.httpClient.Post(ctxT, &resp, url, req, mH)
	if err != nil {
		l.Error("c.httpClient.Post failed", zap.Error(err),
			zap.Any("raw-params", req))

		return err
	}

	if resp.Error != nil {
		l.Error("resp.Error != nil failed", zap.Error(err),
			zap.String("raw-params", req.Params.TransactionID))

		return errors.New(resp.Error.Message)
	}

	var result Result
	if err = resp.Scan(&result); err != nil {
		l.Error("resp.Scan failed", zap.Error(err),
			zap.Any("resp-params", resp))

		return err
	}

	return nil
}
