package dbstore

import (
	"context"
	"database/sql"

	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
)

type contextKey string

const (
	TxContextKey contextKey = "TX_CONTEXT_KEY"
	TxCounterKey contextKey = "Tx_COUNTER"
)

type DBStore struct {
	log  logger.Logger
	db   *sqlx.DB
	tran *TranRepo
}

func New(log logger.Logger, db *sqlx.DB) *DBStore {
	return &DBStore{
		log: log,
		db:  db,
	}
}

func (db *DBStore) TranRepo() *TranRepo {
	if db.tran == nil {
		db.tran = &TranRepo{
			store: db,
		}
	}

	return db.tran
}

func (db *DBStore) TxStart(ctx context.Context) (context.Context, error) {
	l := logger.WithFields(db.log, zap.String("key", "tx_start"))
	counter := ctx.Value(TxCounterKey)

	c, ok := counter.(int)
	if !ok {
		c = 0
	}

	if c == 0 {
		tx, err := db.db.Beginx()
		if err != nil {
			l.Error("db.Beginx failed", zap.Error(err))
			return ctx, err
		}

		l.Debug("db.Beginx - ok")

		ctx = context.WithValue(ctx, TxContextKey, tx)
	}

	l.Debug("tx depth increased", zap.Int("depth", c+1))

	ctx = context.WithValue(ctx, TxCounterKey, c+1)

	return ctx, nil
}

func (db *DBStore) TxFinish(ctx context.Context, err error) error {
	l := logger.WithFields(db.log, zap.String("key", "tx_finish"), zap.Error(err))
	counter := ctx.Value(TxCounterKey)

	c, ok := counter.(int)
	if !ok {
		l.Error("tx depth not found", zap.Error(err))
		return errs.Errf(errs.InternalError, "trying to finish not starting tx")
	}

	if c != 1 {
		l.Debug("tx depth decreased", zap.Int("depth", c-1))
		return nil
	}

	sqlClient := db.sqlClientByCtx(ctx)

	tx, ok := sqlClient.(*sqlx.Tx)
	if !ok {
		l.Error("tx not found in context", zap.Error(err))
		return errs.Errf(errs.InternalError, "transaction not found")
	}

	if err != nil {
		if e := tx.Rollback(); e != nil {
			l.Error("tx.Rollback", zap.Error(e))
			return err
		}

		l.Debug("Rolled back")

		return nil
	}

	if e := tx.Commit(); e != nil {
		l.Error("tx.Commit", zap.Error(e))
		return e
	}

	l.Debug("Committed")

	return nil
}

func (db *DBStore) sqlClientByCtx(ctx context.Context) sqlClient {
	if ctx == nil {
		return db.db
	}
	val := ctx.Value(TxContextKey)
	if val == nil {
		return db.db
	}
	tx, ok := val.(*sqlx.Tx)
	if !ok {
		return db.db
	}
	return tx
}

// sqlClient - common interface for *sqlx.DB and *sqlx.TX
// https://gist.github.com/hielfx/4469d35127d085fc3501d483e34d4bad
type sqlClient interface {
	BindNamed(query string, arg interface{}) (string, []interface{}, error)
	DriverName() string
	Get(dest interface{}, query string, args ...interface{}) error
	GetContext(ctx context.Context, dest interface{}, query string, args ...interface{}) error
	MustExec(query string, args ...interface{}) sql.Result
	Exec(query string, args ...interface{}) (sql.Result, error)
	MustExecContext(ctx context.Context, query string, args ...interface{}) sql.Result
	NamedExec(query string, arg interface{}) (sql.Result, error)
	NamedExecContext(ctx context.Context, query string, arg interface{}) (sql.Result, error)
	NamedQuery(query string, arg interface{}) (*sqlx.Rows, error)
	PrepareNamed(query string) (*sqlx.NamedStmt, error)
	PrepareNamedContext(ctx context.Context, query string) (*sqlx.NamedStmt, error)
	Preparex(query string) (*sqlx.Stmt, error)
	PreparexContext(ctx context.Context, query string) (*sqlx.Stmt, error)
	QueryRowx(query string, args ...interface{}) *sqlx.Row
	QueryRowxContext(ctx context.Context, query string, args ...interface{}) *sqlx.Row
	Queryx(query string, args ...interface{}) (*sqlx.Rows, error)
	QueryxContext(ctx context.Context, query string, args ...interface{}) (*sqlx.Rows, error)
	Rebind(query string) string
	Select(dest interface{}, query string, args ...interface{}) error
	SelectContext(ctx context.Context, dest interface{}, query string, args ...interface{}) error
}
