package dbstore

import (
	"context"
	"errors"
	"fmt"

	"go.uber.org/zap"
)

type TranRepo struct {
	store *DBStore
}

func (dbo *TranRepo) Create(ctx context.Context, tr domains.Transact) (domains.Transact, error) {
	var (
		l         = logger.FromCtx(ctx, "tranRepo.Create")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		q         = `insert into transacts (agent_id, lead_id, "sum", state, oper_date, purpose) 
					 values ($1,$2,$3,$4,$5,$6) returning id;`
	)
	err := sqlClient.GetContext(ctx, &tr.ID, q, tr.AgentID, tr.LeadID, tr.SUM, tr.State, tr.OperDate, tr.Purpose)
	if err != nil {
		l.Error("sqlClient.GetContext failed", zap.Error(err))

		return domains.Transact{}, err
	}

	return tr, nil
}

func (dbo *TranRepo) Get(ctx context.Context, tr domains.Transact) (domains.Transact, error) {
	var (
		l         = logger.FromCtx(ctx, "tranRepo.Get")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		q         = `select * from transacts where id=$1`
		record    dbTran
	)
	if err := sqlClient.GetContext(ctx, &record, q, tr.ID); err != nil {
		l.Error("sqlClient.GetContext failed", zap.Error(err))
		if errors.Is(err, errs.ErrNoRows) {
			return domains.Transact{}, errs.TransactNotFound
		}

		return domains.Transact{}, errs.Errf(errs.InternalError, err.Error())
	}

	return record.toModel(), nil
}

func (dbo *TranRepo) Update(ctx context.Context, tr domains.Transact) error {
	var (
		l         = logger.FromCtx(ctx, "terminalRepo.UpdateTerminal")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		q         = `UPDATE transacts SET state=2
                     WHERE id=$1`
	)
	fmt.Println("tran=> ", tr)
	_, err := sqlClient.Exec(q, tr.ID)
	if err != nil {
		l.Error("sqlClient.Exec failed", zap.Error(err), zap.String("id", tr.ID))

		return errs.Errf(errs.InternalError, err.Error())
	}

	return nil
}

func (dbo *TranRepo) CheckAgent(ctx context.Context, ag domains.Agent) (domains.Agent, error) {
	var (
		l         = logger.FromCtx(ctx, "tranRepo.Create")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		s         = `select * from agents where tax = $1`
		dataA     []dbAgent
	)

	if err := sqlClient.Select(&dataA, s, ag.Tax); err != nil {
		l.Error("sqlClient.Select failed", zap.Error(err))
		if !errors.Is(err, errs.ErrNoRows) {
			return domains.Agent{}, err
		}
	}

	if len(dataA) == 0 {
		return domains.Agent{}, nil
	}

	return dataA[0].toModel(), nil
}

func (dbo *TranRepo) CreateAgent(ctx context.Context, ag domains.Agent) (domains.Agent, error) {
	var (
		l         = logger.FromCtx(ctx, "tranRepo.Create")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		q         = `insert into agents (tax, name) 
					 values ($1,$2) returning *;`
		dataA dbAgent
	)

	if err := sqlClient.GetContext(ctx, &dataA, q, ag.Tax, ag.Name); err != nil {
		l.Error("sqlClient.GetContext failed", zap.Error(err))

		return domains.Agent{}, err
	}

	return dataA.toModel(), nil
}
