package dbstore

import (
	"database/sql"
	"strconv"
)

type dbTran struct {
	ID       string         `db:"id"`
	AgentID  string         `db:"agent_id"`
	LeadID   string         `db:"lead_id"`
	SUM      int            `db:"sum"`
	State    sql.NullInt64  `db:"state"`
	OperDate sql.NullString `db:"oper_date"`
	Purpose  sql.NullString `db:"purpose"`
}

type dbAgent struct {
	ID   string         `db:"id"`
	Tax  string         `db:"tax"`
	Name sql.NullString `db:"name"`
}

func (db *dbTran) toModel() domains.Transact {
	return domains.Transact{
		ID:       db.ID,
		AgentID:  db.AgentID,
		LeadID:   db.LeadID,
		SUM:      strconv.Itoa(db.SUM),
		State:    int(db.State.Int64),
		OperDate: db.OperDate.String,
		Purpose:  db.Purpose.String,
	}
}

func (db *dbAgent) toModel() domains.Agent {
	return domains.Agent{
		ID:   db.ID,
		Tax:  db.Tax,
		Name: db.Name.String,
	}
}
