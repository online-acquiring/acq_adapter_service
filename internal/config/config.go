package config

import (
	"fmt"
)

const ServiceLabel = "acq_adapter_service"

type Config struct {
	Environment string `env:"ENVIRONMENT, default=development"`
	ServiceName string `env:"SERVICE_NAME, default=acq_adapter_service"`
	ServerIP    string `env:"SERVER_IP, default=localhost"`
	LogLevel    string `env:"LOG_LEVEL, default=debug"`
	HTTPPort    string `env:"HTTP_PORT, default=:7002"`

	RMQHost       string `env:"RMQ_URL"`
	RMQExchange   string `env:"RMQ_EXCHANGE"`
	RMQRoutingKey string `env:"RMQ_ROUTING_KEY"`
	RMQQueName    string `env:"RMQ_QUE_NAME"`

	Postgres *DB `env:",prefix=POSTGRES_"`
}

type DB struct {
	Host     string `env:"HOST"`
	Port     int    `env:"PORT"`
	User     string `env:"USER"`
	Password string `env:"PASSWORD"`
	Database string `env:"DATABASE"`
	SSLMode  string `env:"SSLMODE, default=disable"`
	TimeZone string `env:"TIME_ZONE, default=Asia/Tashkent"`
}

func (c *DB) PostgresURL() string {
	if c.User == "" {
		return fmt.Sprintf(
			"host=%s port=%d  dbname=%s sslmode=disable",
			c.Host,
			c.Port,
			c.Database,
		)
	}

	if c.Password == "" {
		return fmt.Sprintf(
			"host=%s port=%d user=%s  dbname=%s sslmode=disable",
			c.Host,
			c.Port,
			c.User,
			c.Database,
		)
	}

	return fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		c.Host,
		c.Port,
		c.User,
		c.Password,
		c.Database,
	)
}
