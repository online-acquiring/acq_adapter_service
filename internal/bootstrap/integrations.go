package bootstrap

import "job/acq_adapter_service/internal/drivers/services/railway"

type integrations struct {
	railways *railway.Client
}

func buildIntegrations(cfg config.Config) integrations {
	return integrations{
		railways: railway.New(cfg),
	}
}
