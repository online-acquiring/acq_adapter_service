package bootstrap

import (
	adapter "job/acq_adapter_service/internal/usecase/check_adapter"
	iabs "job/acq_adapter_service/internal/usecase/check_iabs"
	transource "job/acq_adapter_service/internal/usecase/tran_source"
)

// usecases - container of all the usecases declared in project
type usecases struct {
	tran         *transource.UseCase
	checkIabs    *iabs.UseCase
	checkAdapter *adapter.UseCase
}

func buildUsecases(s dbstore.DBStore, integrate integrations) usecases {
	tranUC := transource.New(s.TranRepo())
	return usecases{
		tran:         tranUC,
		checkIabs:    iabs.New(tranUC, integrate.railways),
		checkAdapter: adapter.New(tranUC),
	}
}
