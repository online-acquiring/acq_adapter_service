package bootstrap

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" // postgresql lib
)

const gracefulDeadline = 5 * time.Second
const rmqReconnectDelayInSeconds = 10

type App struct {
	db       *sqlx.DB
	store    dbstore.DBStore
	teardown []func()
	rest     *http.Server
	mqPool   *mq.Pool
}

func New(cfg config.Config) *App {
	teardown := make([]func(), 0)
	// Setting UP logger
	globalLogger := logger.New(cfg.LogLevel, "acq-adapter-service")
	teardown = append(teardown, func() { _ = logger.Cleanup() })
	globalLogger.Info("Logger initialized")

	log := logger.FromCtx(context.Background(), "bootstrap.New")

	// Подключение к БД
	db, err := sqlx.Connect("postgres", cfg.Postgres.PostgresURL())
	if err != nil {
		panic(err)
	}

	log.Info("Database connection established")
	teardown = append(teardown, func() {
		log.Info("Database connection closing...")
		if err := db.Close(); err != nil {
			log.Error(err.Error())
		}

		log.Info("Database connection closed")
	})

	storage := dbstore.New(log, db)

	log.Info("MQ HOST:" + cfg.RMQHost)

	amqp, err := rabbit.Dial(cfg.RMQHost, log, rmqReconnectDelayInSeconds)
	if err != nil {
		panic(fmt.Sprintf("connection.open: %s", err.Error()))
	}

	log.Info("AMQP connection established")
	teardown = append(teardown, func() {
		log.Info("AMQP connection closing...")
		if err := amqp.Close(); err != nil {
			log.Error(err.Error())
		}

		log.Info("AMQP connection closed")
	})

	integrate := buildIntegrations(cfg)
	useCases := buildUsecases(*storage, integrate)

	httpSrv := rest.New(cfg.HTTPPort, log, &cfg, useCases.checkAdapter)
	teardown = append(teardown, func() {
		log.Info("HTTP is shutting down")
		ctxShutDown, cancel := context.WithTimeout(context.Background(), gracefulDeadline)
		defer cancel()
		if err = httpSrv.Shutdown(ctxShutDown); err != nil {
			log.Error(fmt.Sprintf("server Shutdown Failed:%s", err))
			if err == http.ErrServerClosed {
				err = nil
			}
			return
		}

		log.Info("HTTP is shut down")
	})

	app := App{
		teardown: teardown,
		rest:     httpSrv,
		db:       db,
		store:    *storage,
		mqPool:   mq.NewPool(log, amqp, &cfg, useCases.checkIabs),
	}
	return &app
}

func (app *App) Run(ctx context.Context) {
	go func() {
		if err := app.rest.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			panic(err)
		}
	}()
	go func(c context.Context) {
		app.mqPool.Run(c)
	}(ctx)
	<-ctx.Done()
	for i := range app.teardown {
		app.teardown[i]()
	}
}
