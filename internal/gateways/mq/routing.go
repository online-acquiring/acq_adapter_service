package mq

import (
	"fmt"

	"go.uber.org/zap"
)

type Route struct {
	Exchange   string
	RoutingKey string
	QueueName  string
}

func (p *Pool) endpoints() {
	// here to be added endpoints for mq.
	var w Worker
	r := Route{
		Exchange:   p.cfg.RMQExchange,
		RoutingKey: p.cfg.RMQRoutingKey,
		QueueName:  p.cfg.RMQQueName,
	}

	c, err := consumerDlx(p.conn, r, p.Logger)
	if err != nil {
		panic(err)
	}

	w = New(c, p.Logger, p.tr)

	logRoute(p.Logger, r, w)

	p.workers = append(p.workers, w)
}

func logRoute(l logger.Logger, r Route, w Worker) {
	l.Info(fmt.Sprintf("[MQ] %v/%v/%v", r.Exchange, r.RoutingKey, r.QueueName),
		zap.String("handler", getType(w)))
}

func consumerDlx(c *rabbit.Connection, r Route, l logger.Logger) (*rabbit.Consumer, error) {
	return newDlx(c, r.Exchange, r.RoutingKey, r.QueueName, l)
}
