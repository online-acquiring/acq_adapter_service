package mq

import (
	"context"
	"log"
	"reflect"

	"github.com/streadway/amqp"
)

type Worker interface {
	Run(ctx context.Context) error
}

type Pool struct {
	logger.Logger
	conn    *rabbit.Connection
	workers []Worker
	cfg     *config.Config
	tr      trSource
}

func NewPool(
	l logger.Logger,
	conn *rabbit.Connection,
	cfg *config.Config,
	source trSource,
) *Pool {
	return &Pool{
		Logger:  l,
		conn:    conn,
		workers: make([]Worker, 0),
		cfg:     cfg,
		tr:      source,
	}
}

func getType(handler interface{}) string {
	t := reflect.TypeOf(handler)
	if t.Kind() == reflect.Ptr {
		return "*" + t.Elem().Name()
	}

	return t.Name()
}

func (p *Pool) Run(ctx context.Context) {
	p.endpoints()
	for i := 0; i < len(p.workers); i++ {
		go func(c context.Context, id int) {
			log.Printf("[MQ] Starting worker: %d", id)
			if err := p.workers[id].Run(c); err != nil {
				panic(err)
			}
		}(ctx, i)
	}
	<-ctx.Done()
}

func newDlx(conn *rabbit.Connection, exchange, routingKey, queueName string, l logger.Logger) (*rabbit.Consumer, error) {
	ch, err := conn.Channel()
	if err != nil {
		return nil, err
	}

	err = ch.ExchangeDeclare(exchange, amqp.ExchangeDirect, true, false, false, false, nil)
	if err != nil {
		return nil, err
	}

	q, err := ch.QueueDeclare(queueName, true, false, false, false, nil)
	if err != nil {
		return nil, err
	}
	if err := ch.QueueBind(q.Name, routingKey, exchange, false, nil); err != nil {
		return nil, err
	}

	sub, err := rabbit.NewRawConsumer(conn, q, l)
	if err != nil {
		return nil, err
	}
	return sub, nil
}
