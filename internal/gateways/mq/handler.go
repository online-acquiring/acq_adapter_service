package mq

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/streadway/amqp"
)

type trSource interface {
	CheckFromIabs(ctx context.Context, tran domains.Transact) error
}

type mqReg struct {
	ID         string `json:"id"`
	ClInn      string `json:"clInn"`
	Currday    string `json:"currday"`
	Sumpay     string `json:"sumpay"`
	PayPurpose string `json:"payPurpose"`
}

type Handler struct {
	consumer *rabbit.Consumer
	log      logger.Logger
	ctx      context.Context
	tran     trSource
}

func New(con *rabbit.Consumer, log logger.Logger, tr trSource) *Handler {
	return &Handler{
		consumer: con,
		log:      log,
		tran:     tr,
	}
}

func (w *Handler) Run(ctx context.Context) error {
	w.ctx = ctx
	if err := w.consumer.Run(ctx, "[MQHandler]", w.handle); err != nil {
		w.log.Error(err.Error())
		return err
	}

	return nil
}

func (w *Handler) handle(e amqp.Delivery) {
	w.log.Debug(fmt.Sprintf("[MQHandler]: id(%v) - received", e.MessageId))
	var requeue, ok bool

	defer func() {
		if !ok {
			_ = e.Nack(false, requeue)
			w.log.Debug(fmt.Sprintf("[MQHandler]: id(%v) - Failed", e.MessageId))
		} else {
			w.log.Debug(fmt.Sprintf("[MQHandler]: id(%v) - OK", e.MessageId))
		}
	}()

	var msg mqReg
	if err := json.Unmarshal(e.Body, &msg); err != nil {
		w.log.Error(err.Error())
		ok = false

		return
	}

	tr := domains.Transact{
		AgentID:  msg.ClInn,
		LeadID:   msg.ID,
		SUM:      msg.Sumpay,
		State:    domains.TransactCreated,
		OperDate: msg.Currday,
		Purpose:  msg.PayPurpose,
	}
	err := w.tran.CheckFromIabs(w.ctx, tr)
	if err != nil {
		w.log.Error(err.Error())
		ok = false

		return
	}

	if err := e.Ack(false); err != nil {
		w.log.Error("AckError:" + err.Error())
	}

	ok = true
}
