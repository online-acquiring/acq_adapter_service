package rest

import (
	"context"
	"fmt"
	"net/http"
	"time"
)

type tranSource interface {
	CheckFromAdapter(ctx context.Context, tran domains.Transact, ag domains.Agent) (domains.Transact, *errs.Error)
}

type Server struct {
	log    logger.Logger
	router *gin.Engine
	cfg    *config.Config
	trn    tranSource
}

func New(port string, log logger.Logger,
	cfg *config.Config, tr tranSource) *http.Server {
	r := gin.New()
	r.Use(cors.CORSMiddleware())
	srv := &Server{
		log:    log,
		router: r,
		cfg:    cfg,
		trn:    tr,
	}

	srv.endpoints()
	httpServer := &http.Server{
		Addr:              port,
		Handler:           srv,
		ReadHeaderTimeout: 70 * time.Second,
	}
	srv.log.Info(fmt.Sprintf("HTTP server is initialized on port: %v", port))
	return httpServer
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}
