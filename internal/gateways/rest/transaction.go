package rest

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
)

type response struct {
	TransactionID string `json:"transactionId"`
	State         int    `json:"state"`
	INN           string `json:"inn"`
}

// check godoc
// @Summary get transaction info
// @Description get transaction info
// @Router /api/v1/check [GET]
// @Tags Transaction
// @Accept json
// @Produce json
// @Param id query string true "id transact"
// @Param inn query string true "inn transact"
// @Success 200 {object} R{data=response}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) check() gin.HandlerFunc {
	return func(c *gin.Context) {
		tranID := c.Query("id")
		if tranID == "" {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: "Ид не передан",
			})

			return
		}

		inn := c.Query("inn")
		if inn == "" {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: "Инн не передан",
			})

			return
		}

		tran, err := s.trn.CheckFromAdapter(context.Background(), domains.Transact{ID: tranID}, domains.Agent{Tax: inn})
		if err != nil {
			c.JSON(http.StatusUnprocessableEntity, errView(err.Code, err.Message))

			return
		}

		resp := response{
			TransactionID: tran.ID,
			State:         domains.TransactConfirmed,
			INN:           tran.AgentID,
		}

		c.JSON(http.StatusOK, view(resp))
	}
}
