CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
---agents
create table agents
(
    id         uuid                 default uuid_generate_v4() not null
        constraint agents_pk
            primary key,
    tax       varchar(64) not null,
    name       varchar(100)
);

--- transacts
create table transacts
(
    id             uuid                     default uuid_generate_v4() not null
        constraint transacts_pk
            primary key,
    agent_id uuid constraint transacts_agents_id_fk references agents,
    lead_id       varchar(50)                                        not null,
    sum     numeric                                         not null,
    state          integer,
    oper_date          varchar(20),
    purpose varchar(500)
);
